# Introducción a C#

Introducción al lenguaje de programación C#

### Pre-requisitos 📋

- [1] _Visual Studio_

### Instalación 🔧

- [1] Pasos

## Despliegue 📦

_Agrega notas adicionales sobre como hacer deploy_

## Construido con 🛠️

_Menciona las herramientas que utilizaste para crear tu proyecto_

* [Dropwizard](https://www.dropwizard.io/1.0.2/docs/) - El framework web usado
* [Maven](https://maven.apache.org/) - Manejador de dependencias
* [ROME](https://rometools.github.io/rome/) - Usado para generar RSS

## Wiki 📖

Puedes encontrar mucho más de cómo utilizar este proyecto en nuestra [Wiki](https://gitlab.com/halexisgonzalez/intro_csharp/-/wikis/Introducci%C3%B3n-a-C%23)

## Versionado 📌

Usamos [SemVer](https://semver.org/) para el versionado. Para todas las versiones disponibles, mira los [tags en este repositorio](https://github.com/tu/proyecto/tags).

## Fuente 📜

* **PildorasInformaticas** - *Base del Curso* - [PildorasInformáticas](https://www.youtube.com/@pildorasinformaticas)

---